import requests
import json
import string
import hashlib
import random
from token import *
from configs import *
from database import *


def get_user_info(user_id):
    params = {'user_id': user_id,
              'access_token': ACCESS_TOKEN,
              'v': API_VERSION}
    response = requests.get('https://api.vk.com/method/users.get', params=params).json()
    if 'error' in response:
        print(response)
    return response['response'][0]


def send_message(user_id, text, keyboard=None, attachment=None):
    params = {'access_token': ACCESS_TOKEN,
              'random_id': random.randint(0, 1e10),
              'group_id': GROUP_ID,
              'user_id': user_id,
              'message': text,
              'v': API_VERSION,
              'attachment': attachment}
    if keyboard is not None:
        params['keyboard'] = json.dumps(keyboard, ensure_ascii=False).encode('utf8')

    response = requests.get('https://api.vk.com/method/messages.send', params=params).json()
    if 'error' in response:
        print(response)


def main():
    params = {'access_token': ACCESS_TOKEN,
              'group_id': GROUP_ID,
              'v': API_VERSION}
    response = requests.get('https://api.vk.com/method/groups.getLongPollServer', params=params).json()
    if 'error' in response:
        print(response)

    key, server, ts = response['response']['key'], response['response']['server'], response['response']['ts']

    db = Database()
    db.create_table()

    while True:
        params = {'act': 'a_check',
                  'key': key,
                  'ts': ts,
                  'wait': 0.1}
        response = requests.get(server, params=params).json()
        if 'error' in response:
            print(response)

        ts = response['ts']

        for update in response['updates']:
            print(update)
            if update['type'] == 'message_new':
                message = update['object']['text'].capitalize()
                user_id = update['object']['from_id']
                user_info = get_user_info(user_id)

                if message == 'Начать' or db.is_new(user_id):
                    send_message(user_id, FIRST_MEETING_MESSAGE, START_KEYBOARD)
                    send_message(ADMIN_ID,
                                 f"Новое сообщение в клинике от пользователя {user_info['first_name']} {user_info['last_name']}")
                    if db.is_new(user_id):
                        db.add_new_user(user_id, user_info['first_name'], user_info['last_name'])
                elif message == 'Товары':
                    send_message(user_id, SHOP_MESSAGE, SHOP_KEYBOARD)
                elif message == 'Котики':
                    picture_id = random.randint(0, len(IMAGES_ID))
                    send_message(user_id, CATS_MESSAGE, None, IMAGES_ID[picture_id])
                elif message == 'Статьи':
                    send_message(user_id, WORKS_MESSAGE, WORKS_KEYBOARD, VET_PHOTO)
                elif message == 'Акции':
                    send_message(user_id, SALES_MESSAGE)
                elif message == 'Контакты':
                    send_message(user_id, CONTACT_MESSAGE)
                elif message == 'Назад':
                    send_message(user_id, SECOND_MEETING_MESSAGE, START_KEYBOARD)
                else:
                    send_message(ADMIN_ID,
                                 f"Новое сообщение в клинике от пользователя {user_info['first_name']} {user_info['last_name']}")


if __name__ == '__main__':
    main()
