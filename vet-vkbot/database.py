import sqlite3


class Database:
    def __init__(self):
        self.conn = sqlite3.connect('clients.db')
        self.cur = self.conn.cursor()

    def create_table(self):
        self.cur.execute('''
                   CREATE TABLE IF NOT EXISTS users (
                       id INTEGER,
                       first_name VARCHAR(255),
                       last_name VARCHAR(255)
                   )''')
        self.conn.commit()

    def is_new(self, user_id):
        self.cur.execute(f'''SELECT EXISTS(SELECT id FROM users WHERE id = {user_id})''')
        return 1 - self.cur.fetchone()[0]

    def add_new_user(self, user_id, first_name, last_name):
        self.cur.execute(
            f'''INSERT INTO users VALUES ('{user_id}', '{first_name}', '{last_name}')''')
        self.conn.commit()

    def get_all_users(self):
        self.cur.execute('''SELECT first_name FROM users''')
        a = self.cur.fetchall()
        self.cur.execute('''SELECT last_name FROM users''')
        b = self.cur.fetchall()
        message = ''
        for i in range(len(a)):
            message += a[i][0] + ' ' + b[i][0] + '\n'
        return message
